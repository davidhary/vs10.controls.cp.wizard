﻿#Region "Copyright ©2005, Cristi Potlog - All Rights Reserved"
' ------------------------------------------------------------------- *
'*                            Cristi Potlog                             *
'*                  Copyright ©2005 - All Rights reserved               *
'*                                                                      *
'* THIS SOURCE CODE IS PROVIDED "AS IS" WITH NO WARRANTIES OF ANY KIND, *
'* EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE        *
'* WARRANTIES OF DESIGN, MERCHANTIBILITY AND FITNESS FOR A PARTICULAR   *
'* PURPOSE, NONINFRINGEMENT, OR ARISING FROM A COURSE OF DEALING,       *
'* USAGE OR TRADE PRACTICE.                                             *
'*                                                                      *
'* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.             *
'* ------------------------------------------------------------------- 
#End Region

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> 
Partial Class Wizard
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> 
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                If Me.components IsNot Nothing Then
                    Me.components.Dispose()
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> 
    Private Sub InitializeComponent()
        Me._CancelButton = New System.Windows.Forms.Button()
        Me._NextButton = New System.Windows.Forms.Button()
        Me._BackButton = New System.Windows.Forms.Button()
        Me._HelpButton = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        ' 
        ' Me._CancelButton
        ' 
        Me._CancelButton.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
        Me._CancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me._CancelButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._CancelButton.Location = New System.Drawing.Point(344, 224)
        Me._CancelButton.Name = "_CancelButton"
        Me._CancelButton.TabIndex = 8
        Me._CancelButton.Text = "Cancel"
        '			Me.buttonCancel.Click += New System.EventHandler(Me.buttonCancel_Click)
        ' 
        ' Me._NextButton
        ' 
        Me._NextButton.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
        Me._NextButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._NextButton.Location = New System.Drawing.Point(260, 224)
        Me._NextButton.Name = "_NextButton"
        Me._NextButton.TabIndex = 7
        Me._NextButton.Text = "&Next >"
        '			Me.buttonNext.Click += New System.EventHandler(Me.buttonNext_Click)
        ' 
        ' Me._BackButton
        ' 
        Me._BackButton.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
        Me._BackButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._BackButton.Location = New System.Drawing.Point(184, 224)
        Me._BackButton.Name = "_BackButton"
        Me._BackButton.TabIndex = 6
        Me._BackButton.Text = "< &Back"
        '			Me.buttonBack.Click += New System.EventHandler(Me.buttonBack_Click)
        ' 
        ' Me._HelpButton
        ' 
        Me._HelpButton.Anchor = (CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles))
        Me._HelpButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._HelpButton.Location = New System.Drawing.Point(8, 224)
        Me._HelpButton.Name = "_HelpButton"
        Me._HelpButton.TabIndex = 9
        Me._HelpButton.Text = "&Help"
        Me._HelpButton.Visible = False
        '			Me.buttonHelp.Click += New System.EventHandler(Me.buttonHelp_Click)
        ' 
        ' Wizard
        ' 
        Me.Controls.Add(Me._HelpButton)
        Me.Controls.Add(Me._CancelButton)
        Me.Controls.Add(Me._NextButton)
        Me.Controls.Add(Me._BackButton)
        Me.Name = "Wizard"
        Me.Size = New System.Drawing.Size(428, 256)
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _CancelButton As System.Windows.Forms.Button
    Private WithEvents _NextButton As System.Windows.Forms.Button
    Private WithEvents _BackButton As System.Windows.Forms.Button
    Private WithEvents _HelpButton As System.Windows.Forms.Button


End Class