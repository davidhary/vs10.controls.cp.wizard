#Region " IMPORTS "
Imports System.Collections
Imports System.ComponentModel
Imports System.Windows.Forms.Design
Imports System.ComponentModel.Design
#End Region

''' <summary>
''' Represents an extendable wizard control with basic page navigation functionality.
''' </summary>
''' <license>
''' (c) 2012 Integrated Scientific Resources, Inc., All Rights Reserved
''' (c) 2005 Cristi Potlog - All Rights Reserved
''' Licensed under the MIT License. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="09/19/2012" by="David" revision="1.05.4645.x">
''' Based on http://www.CodeProject.com/Articles/10808/Cristi-Potlog-s-Wizard-Control-for-NET
''' </history>
<Designer(GetType(Wizard.WizardDesigner))> 
Public Class Wizard
    Inherits System.Windows.Forms.UserControl

#Region " CONSTANTS "

    Private Const footerAreaHeight As Integer = 48
    Private ReadOnly offsetCancel As New Point(84, 36)
    Private ReadOnly offsetNext As New Point(168, 36)
    Private ReadOnly offsetBack As New Point(244, 36)

#End Region

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Creates a new instance of the <see cref="Wizard"/> class.
    ''' </summary>
    Public Sub New()
        ' call required by designer
        Me.InitializeComponent()

        ' reset control style to improve rendering (reduce flicker)
        MyBase.SetStyle(ControlStyles.AllPaintingInWmPaint, True)
        MyBase.SetStyle(ControlStyles.DoubleBuffer, True)
        MyBase.SetStyle(ControlStyles.ResizeRedraw, True)
        MyBase.SetStyle(ControlStyles.UserPaint, True)

        ' reset dock style
        MyBase.Dock = DockStyle.Fill

        ' initialize pages collection
        Me._pages = New WizardPagesCollection(Me)

        Me.CancelText = "&Cancel"
    End Sub

#End Region

#Region " PROPERTIES "

    ''' <summary>
    ''' Gets or sets which edge of the parent container a control is docked to.
    ''' </summary>
    <DefaultValue(DockStyle.Fill), Category("Layout"), Description("Gets or sets which edge of the parent container a control is docked to.")> 
    Public Shadows Property Dock() As DockStyle
        Get
            Return MyBase.Dock
        End Get
        Set(ByVal value As DockStyle)
            MyBase.Dock = value
        End Set
    End Property

    Private _pages As WizardPagesCollection
    ''' <summary>
    ''' Gets the collection of wizard pages in this tab control.
    ''' </summary>
    <Category("Wizard"), DesignerSerializationVisibility(DesignerSerializationVisibility.Content), Description("Gets the collection of wizard pages in this tab control.")> 
    Public ReadOnly Property Pages() As WizardPagesCollection
        Get
            Return Me._pages
        End Get
    End Property

    Private _NewPage As WizardPage
    ''' <summary>
    ''' Gets the New page -- the page to be displayed.
    ''' Selected when the <see cref="PageChanged">page changed event</see> occurs but before it is invoked.
    ''' </summary>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)> 
    Public ReadOnly Property NewPage() As WizardPage
        Get
            Return Me._NewPage
        End Get
    End Property

    Private _OldPage As WizardPage
    ''' <summary>
    ''' Gets the old page -- the page already displayed.
    ''' Selected when the <see cref="PageChanged">page changed event</see> occurs but before it is invoked.
    ''' </summary>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)> 
    Public ReadOnly Property OldPage() As WizardPage
        Get
            Return Me._OldPage
        End Get
    End Property

    Private _selectedPage As WizardPage
    ''' <summary>
    ''' Gets or sets the currently-selected wizard page.
    ''' </summary>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)> 
    Public Property SelectedPage() As WizardPage
        Get
            Return Me._selectedPage
        End Get
        Set(ByVal value As WizardPage)
            ' select new page
            Me.ActivatePage(value)
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the currently-selected wizard page by index.
    ''' </summary>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)> 
    Friend Property SelectedIndex() As Integer
        Get
            Return Me._pages.IndexOf(Me._selectedPage)
        End Get
        Set(ByVal value As Integer)
            ' check if there are any pages or index out of range.
            If Me._pages.Count = 0 OrElse value < -1 OrElse value >= Me._pages.Count Then
                ' reset invalid index
                Me.ActivatePage(-1)
            Else
                ' select new page
                Me.ActivatePage(value)
            End If
        End Set
    End Property

    Private _headerImage As Image
    ''' <summary>
    ''' Gets or sets the image displayed on the header of the standard pages.
    ''' </summary>
    <System.ComponentModel.DefaultValue(CType(Nothing, Object)), Category("Wizard"), Description("Gets or sets the image displayed on the header of the standard pages.")> 
    Public Property HeaderImage() As Image
        Get
            Return Me._headerImage
        End Get
        Set(ByVal value As Image)
            If Me._headerImage IsNot value Then
                Me._headerImage = value
                Me.Invalidate()
            End If
        End Set
    End Property

    Private _welcomeImage As Image
    ''' <summary>
    ''' Gets or sets the image displayed on the welcome and finish pages.
    ''' </summary>
    <System.ComponentModel.DefaultValue(CType(Nothing, Object)), Category("Wizard"), Description("Gets or sets the image displayed on the welcome and finish pages.")> 
    Public Property WelcomeImage() As Image
        Get
            Return Me._welcomeImage
        End Get
        Set(ByVal value As Image)
            If Me._welcomeImage IsNot value Then
                Me._welcomeImage = value
                Me.Invalidate()
            End If
        End Set
    End Property


    Private _headerFont As Font
    ''' <summary>
    ''' Gets or sets the font used to display the description of a standard page.
    ''' </summary>
    <Category("Appearance"), Description("Gets or sets the font used to display the description of a standard page.")> 
    Public Property HeaderFont() As Font
        Get
            If Me._headerFont Is Nothing Then
                Return Me.Font
            Else
                Return Me._headerFont
            End If
        End Get
        Set(ByVal value As Font)
            If Me._headerFont IsNot value Then
                Me._headerFont = value
                Me.Invalidate()
            End If
        End Set
    End Property

    Protected Function ShouldSerializeHeaderFont() As Boolean
        Return Me._headerFont IsNot Nothing
    End Function

    Private _headerTitleFont As Font
    ''' <summary>
    ''' Gets or sets the font used to display the title of a standard page.
    ''' </summary>
    <Category("Appearance"), Description("Gets or sets the font used to display the title of a standard page.")> 
    Public Property HeaderTitleFont() As Font
        Get
            If Me._headerTitleFont Is Nothing Then
                Return New Font(Me.Font.FontFamily, Me.Font.Size + 2, FontStyle.Bold)
            Else
                Return Me._headerTitleFont
            End If
        End Get
        Set(ByVal value As Font)
            If Me._headerTitleFont IsNot value Then
                Me._headerTitleFont = value
                Me.Invalidate()
            End If
        End Set
    End Property

    ''' <summary>
    ''' Returns the sentinel indicating if header title font should be serialized.
    ''' </summary><returns></returns>
    Protected Function ShouldSerializeHeaderTitleFont() As Boolean
        Return Me._headerTitleFont IsNot Nothing
    End Function

    Private _welcomeFont As Font
    ''' <summary>
    ''' Gets or sets the font used to display the description of a welcome of finish page.
    ''' </summary>
    <Category("Appearance"), Description("Gets or sets the font used to display the description of a welcome of finish page.")> 
    Public Property WelcomeFont() As Font
        Get
            If Me._welcomeFont Is Nothing Then
                Return Me.Font
            Else
                Return Me._welcomeFont
            End If
        End Get
        Set(ByVal value As Font)
            If Me._welcomeFont IsNot value Then
                Me._welcomeFont = value
                Me.Invalidate()
            End If
        End Set
    End Property

    ''' <summary>
    ''' Returns the sentinel indicating if welcome font should be serialized.
    ''' </summary><returns></returns>
    Protected Function ShouldSerializeWelcomeFont() As Boolean
        Return Me._welcomeFont IsNot Nothing
    End Function

    Private _welcomeTitleFont As Font
    ''' <summary>
    ''' Gets or sets the font used to display the title of a welcome of finish page.
    ''' </summary>
    <Category("Appearance"), Description("Gets or sets the font used to display the title of a welcome of finish page.")> 
    Public Property WelcomeTitleFont() As Font
        Get
            If Me._welcomeTitleFont Is Nothing Then
                Return New Font(Me.Font.FontFamily, Me.Font.Size + 10, FontStyle.Bold)
            Else
                Return Me._welcomeTitleFont
            End If
        End Get
        Set(ByVal value As Font)
            If Me._welcomeTitleFont IsNot value Then
                Me._welcomeTitleFont = value
                Me.Invalidate()
            End If
        End Set
    End Property

    Protected Function ShouldSerializeWelcomeTitleFont() As Boolean
        Return Me._welcomeTitleFont IsNot Nothing
    End Function

    ''' <summary>
    ''' Gets or sets the enabled state of the Next button. 
    ''' </summary>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)> 
    Public Property NextEnabled() As Boolean
        Get
            Return Me._NextButton.Enabled
        End Get
        Set(ByVal value As Boolean)
            Me._NextButton.Enabled = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the enabled state of the back button. 
    ''' </summary>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)> 
    Public Property BackEnabled() As Boolean
        Get
            Return Me._BackButton.Enabled
        End Get
        Set(ByVal value As Boolean)
            Me._BackButton.Enabled = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the enabled state of the cancel button. 
    ''' </summary>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)> 
    Public Property CancelEnabled() As Boolean
        Get
            Return Me._CancelButton.Enabled
        End Get
        Set(ByVal value As Boolean)
            Me._CancelButton.Enabled = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the visible state of the help button. 
    ''' </summary>
    <Category("Behavior"), DefaultValue(False), Description("Gets or sets the visible state of the help button.")> 
    Public Property HelpVisible() As Boolean
        Get
            Return Me._HelpButton.Visible
        End Get
        Set(ByVal value As Boolean)
            Me._HelpButton.Visible = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the text displayed by the Next button. 
    ''' </summary>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)> 
    Public Property NextText() As String
        Get
            Return Me._NextButton.Text
        End Get
        Set(ByVal value As String)
            Me._NextButton.Text = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the text displayed by the back button. 
    ''' </summary>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)> 
    Public Property BackText() As String
        Get
            Return Me._BackButton.Text
        End Get
        Set(ByVal value As String)
            Me._BackButton.Text = value
        End Set
    End Property

#If False Then
    ''' <summary>
    ''' Gets or sets the text displayed by the cancel button. 
    ''' </summary>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)> 
    Public Property CancelText() As String
        Get
            Return Me._CancelButton.Text
        End Get
        Set(ByVal value As String)
            Me._CancelButton.Text = value
        End Set
    End Property
#End If

    ''' <summary>
    ''' Gets or sets the text displayed by the cancel button. 
    ''' </summary>
    <Browsable(True), Category("Appearance"), DefaultValue("&Cancel"),
    Description("Gets or sets the test to display on the title of the Cancel button.")> 
    Public Property CancelText() As String

    ''' <summary>
    ''' Gets or sets the text displayed by the cancel button. 
    ''' </summary>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)> 
    Public Property HelpText() As String
        Get
            Return Me._HelpButton.Text
        End Get
        Set(ByVal value As String)
            Me._HelpButton.Text = value
        End Set
    End Property

    Private _finishText As String
    ''' <summary>
    ''' Gets or sets the text displayed by the Finish button. 
    ''' </summary>
    <Browsable(True), Category("Appearance"), DefaultValue("&Finish"),
    Description("Gets or sets the test to display on the title of Finish button on the finish page.")> 
    Public Property FinishText() As String
        Get
            Return Me._finishText
        End Get
        Set(value As String)
            Me._finishText = value
        End Set
    End Property

#End Region

#Region " METHODS "

    ''' <summary>
    ''' Advances to next wizard page.
    ''' </summary>
    Public Sub [Next]()
        ' check if we're on the last page (finish)
        If Me.SelectedIndex = Me._pages.Count - 1 Then
            Me._NextButton.Enabled = False
        Else
            ' handle page change
            Me.OnPageChanging(New PageChangingEventArgs(Me.SelectedIndex, Me.SelectedIndex + 1))
        End If
    End Sub

    ''' <summary>
    ''' Retreats to the previous wizard page.
    ''' </summary>
    Public Sub Back()
        If Me.SelectedIndex = 0 Then
            Me._BackButton.Enabled = False
        Else
            ' handle page change
            Me.OnPageChanging(New PageChangingEventArgs(Me.SelectedIndex, Me.SelectedIndex - 1))
        End If
    End Sub

    ''' <summary>
    ''' Activates the specified wizard page.
    ''' </summary>
    ''' <param name="index">An Integer value representing the zero-based index of the page to be activated.</param>
    Private Sub ActivatePage(ByVal index As Integer)
        ' check if new page is invalid
        If index < 0 OrElse index >= Me._pages.Count Then
            ' filter out
            Return
        End If

        ' get new page
        Dim page As WizardPage = CType(Me._pages(index), WizardPage)

        ' activate page
        Me.ActivatePage(page)

    End Sub

    ''' <summary>
    ''' Activates the specified wizard page.
    ''' </summary>
    ''' <param name="page">A WizardPage object representing the page to be activated.</param>
    Private Sub ActivatePage(ByVal page As WizardPage)

        ' validate given page
        If Me._pages.Contains(page) = False Then
            ' filter out
            Return
        End If

        ' deactivate current page
        If Me._selectedPage IsNot Nothing Then
            Me._selectedPage.Visible = False
        End If

        ' activate new page
        Me._selectedPage = page

        If Me._selectedPage IsNot Nothing Then
            'Ensure that this panel displays inside the wizard
            Me._selectedPage.Parent = Me
            If Me.Contains(Me._selectedPage) = False Then
                Me.Container.Add(Me._selectedPage)
            End If
            If Me._selectedPage.WizardPageStyle = WizardPageStyle.Finish Then
                Me._CancelButton.Text = Me.FinishText
                Me._CancelButton.DialogResult = DialogResult.OK
            Else
                Me._CancelButton.Text = Me.CancelText ' "Cancel"
                Me._CancelButton.DialogResult = DialogResult.Cancel
            End If

            'Make it fill the space
            Me._selectedPage.SetBounds(0, 0, Me.Width, Me.Height - footerAreaHeight)
            Me._selectedPage.Visible = True
            Me._selectedPage.BringToFront()
            Wizard.FocusFirstTabIndex(Me._selectedPage)
        End If

        'What should the back button say
        If Me.SelectedIndex > 0 Then
            Me._BackButton.Enabled = True
        Else
            Me._BackButton.Enabled = False
        End If

        'What should the Next button say
        If Me.SelectedIndex < Me._pages.Count - 1 Then
            Me._NextButton.Enabled = True
        Else
            If Me.DesignMode = False Then
                ' at runtime disable back button (we finished; there's no point going back)
                Me._BackButton.Enabled = False
            End If
            Me._NextButton.Enabled = False
        End If

        ' refresh
        If Me._selectedPage IsNot Nothing Then
            Me._selectedPage.Invalidate()
        Else
            Me.Invalidate()
        End If
    End Sub

    ''' <summary>
    ''' Focus the control with a lowest tab index in the given container.
    ''' </summary>
    ''' <param name="container">A Control object to pe processed.</param>
    Private Shared Sub FocusFirstTabIndex(ByVal container As Control)
        ' initialize search result variable
        Dim searchResult As Control = Nothing

        ' find the control with the lowest tab index
        For Each control As Control In container.Controls
            If control.CanFocus AndAlso (searchResult Is Nothing OrElse control.TabIndex < searchResult.TabIndex) Then
                searchResult = control
            End If
        Next control

        ' check if anything searchResult
        If searchResult IsNot Nothing Then
            ' focus found control
            searchResult.Focus()
        Else
            ' focus the container
            container.Focus()
        End If
    End Sub

    ''' <summary>
    ''' Raises the <see cref="PageChanging">page changing event</see>.
    ''' </summary>
    ''' <param name="e">A WizardPageEventArgs object that holds event data.</param>
    Protected Overridable Sub OnPageChanging(ByVal e As PageChangingEventArgs)

        If e Is Nothing Then Return

        ' get wizard page already displayed
        Me._OldPage = Me.Pages(e.OldIndex)

        ' get wizard page to be displayed
        Me._NewPage = Me.Pages(e.NewIndex)

        ' check if there are subscribers and raise changing event
        If PageChangingEvent IsNot Nothing Then PageChangingEvent.Invoke(Me, e)

        ' check if user canceled
        If e.Cancel Then
            ' filter
            Return
        End If

        ' activate new page
        Me.ActivatePage(e.NewIndex)

        ' raise the changed
        Me.OnPageChanged(TryCast(e, PageChangedEventArgs))
    End Sub

    ''' <summary>
    ''' Raises the <see cref="PageChanged">page changed event</see>..
    ''' </summary>
    ''' <param name="e">A WizardPageEventArgs object that holds event data.</param>
    Protected Overridable Sub OnPageChanged(ByVal e As PageChangedEventArgs)

        If e Is Nothing Then Return

        ' get wizard page already displayed
        Me._OldPage = Me.Pages(e.OldIndex)

        ' get wizard page to be displayed
        Me._NewPage = Me.Pages(e.NewIndex)

        ' check if there are subscribers and raise the changed event
        If PageChangedEvent IsNot Nothing Then PageChangedEvent.Invoke(Me, e)
    End Sub

    ''' <summary>
    ''' Raises the Cancel event.
    ''' </summary>
    ''' <param name="e">A CancelEventArgs object that holds event data.</param>
    Protected Overridable Sub OnCancel(ByVal e As CancelEventArgs)
        If e Is Nothing Then Return
        ' check if there are subscribers and raise Cancel event
        If CancelEvent IsNot Nothing Then CancelEvent.Invoke(Me, e)
    End Sub

    ''' <summary>
    ''' Raises the Finish event.
    ''' </summary>
    ''' <param name="e">A EventArgs object that holds event data.</param>
    Protected Overridable Sub OnFinish(ByVal e As EventArgs)
        If e Is Nothing Then Return
        ' check if there are subscribers and raise Finish event
        If FinishEvent IsNot Nothing Then FinishEvent.Invoke(Me, e)
    End Sub

    ''' <summary>
    ''' Raises the Help event.
    ''' </summary>
    ''' <param name="e">A EventArgs object that holds event data.</param>
    Protected Overridable Sub OnHelp(ByVal e As EventArgs)
        If e Is Nothing Then Return
        ' check if there are subscribers and raise the Help event
        If HelpEvent IsNot Nothing Then HelpEvent.Invoke(Me, e)
    End Sub

    ''' <summary>
    ''' Raises the Load event.
    ''' </summary>
    Protected Overrides Sub OnLoad(ByVal e As EventArgs)

        ' raise the Load event
        MyBase.OnLoad(e)

        ' activate first page, if exists
        If Me._pages.Count > 0 Then
            Me.ActivatePage(0)
        End If
    End Sub

    ''' <summary>
    ''' Raises the Resize event.
    ''' </summary>
    Protected Overrides Sub OnResize(ByVal e As EventArgs)
        ' raise the Resize event
        MyBase.OnResize(e)

        ' resize the selected page to fit the wizard
        If Me._selectedPage IsNot Nothing Then
            Me._selectedPage.SetBounds(0, 0, Me.Width, Me.Height - footerAreaHeight)
        End If

        ' position navigation buttons
        Me._CancelButton.Location = New Point(Me.Width - Me.offsetCancel.X, Me.Height - Me.offsetCancel.Y)
        Me._NextButton.Location = New Point(Me.Width - Me.offsetNext.X, Me.Height - Me.offsetNext.Y)
        Me._BackButton.Location = New Point(Me.Width - Me.offsetBack.X, Me.Height - Me.offsetBack.Y)
        Me._HelpButton.Location = New Point(Me._HelpButton.Location.X, Me.Height - Me.offsetBack.Y)
    End Sub

    ''' <summary>
    ''' Raises the Paint event.
    ''' </summary>
    Protected Overrides Sub OnPaint(ByVal e As PaintEventArgs)
        ' raise the Paint event
        MyBase.OnPaint(e)
        If e Is Nothing Then Return
        Dim bottomRect As Rectangle = Me.ClientRectangle
        bottomRect.Y = Me.Height - footerAreaHeight
        bottomRect.Height = footerAreaHeight
        ControlPaint.DrawBorder3D(e.Graphics, bottomRect, Border3DStyle.Etched, Border3DSide.Top)
    End Sub

    ''' <summary>
    ''' Raises the ControlAdded event.
    ''' </summary>
    Protected Overrides Sub OnControlAdded(ByVal e As ControlEventArgs)
        ' prevent other controls from being added directly to the wizard
        If e Is Nothing Then Return
        If TypeOf e.Control Is WizardPage = False AndAlso e.Control IsNot Me._CancelButton AndAlso e.Control IsNot Me._NextButton AndAlso e.Control IsNot Me._BackButton Then
            ' add the control to the selected page
            If Me._selectedPage IsNot Nothing Then
                Me._selectedPage.Controls.Add(e.Control)
            End If
        Else
            ' raise the ControlAdded event
            MyBase.OnControlAdded(e)
        End If
    End Sub

#End Region

#Region " EVENTS "

    ''' <summary>
    ''' Occurs before the wizard page changes, giving the user a chance to validate.
    ''' </summary>
    <Category("Wizard"), Description("Occurs before the wizard page changes, giving the user a chance to validate.")> 
    Public Event PageChanging As EventHandler(Of PageChangingEventArgs)

    ''' <summary>
    ''' Occurs after the wizard page changed, giving the user a chance to setup the new page.
    ''' </summary>
    <Category("Wizard"), Description("Occurs after the wizard page changed, giving the user a chance to setup the new page.")> 
    Public Event PageChanged As EventHandler(Of PageChangedEventArgs)

    ''' <summary>
    ''' Occurs when wizard is canceled, giving the user a chance to validate.
    ''' </summary>
    <Category("Wizard"), Description("Occurs when wizard is canceled, giving the user a chance to validate.")> 
    Public Event Cancel As CancelEventHandler
    ''' <summary>
    ''' Occurs when wizard is finished, giving the user a chance to do extra stuff.
    ''' </summary>
    <Category("Wizard"), Description("Occurs when wizard is finished, giving the user a chance to do extra stuff.")> 
    Public Event Finish As EventHandler
    ''' <summary>
    ''' Occurs when the user clicks the help button.
    ''' </summary>
    <Category("Wizard"), Description("Occurs when the user clicks the help button.")> 
    Public Event Help As EventHandler

#End Region

#Region " EVENTS HANDLERS "

    ''' <summary>
    ''' Handles the Click event of the Next button.
    ''' </summary>
    Private Sub _NextButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _NextButton.Click
        Me.Next()
    End Sub

    ''' <summary>
    ''' Handles the Click event of the Back button.
    ''' </summary>
    Private Sub _BackButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _BackButton.Click
        Me.Back()
    End Sub

    ''' <summary>
    ''' Handles the Click event of the Cancel button.
    ''' </summary>
    Private Sub _CancelButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _CancelButton.Click
        ' check if button is cancel mode
        If Me._CancelButton.DialogResult = DialogResult.Cancel Then
            Me.OnCancel(New CancelEventArgs())
            ' check if button is finish mode
        ElseIf Me._CancelButton.DialogResult = DialogResult.OK Then
            Me.OnFinish(EventArgs.Empty)
        End If
    End Sub

    ''' <summary>
    ''' Handles the Click event of the Help button.
    ''' </summary>
    Private Sub _HelpButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _HelpButton.Click
        Me.OnHelp(EventArgs.Empty)
    End Sub

#End Region

#Region " INNER CLASSES "

    ''' <summary>
    ''' Represents a designer for the wizard control.
    ''' </summary>
    Friend Class WizardDesigner
        Inherits ParentControlDesigner

#Region " METHODS "
        ''' <summary>
        ''' Overrides the handling of Mouse clicks to allow back-next to work in the designer.
        ''' </summary>
        ''' <param name="msg">A Message value.</param>
        Protected Overrides Sub WndProc(ByRef msg As Message)
            ' declare PInvoke constants
            Const WM_LBUTTONDOWN As Integer = &H201
            Const WM_LBUTTONDBLCLK As Integer = &H203

            ' check message
            If msg.Msg = WM_LBUTTONDOWN OrElse msg.Msg = WM_LBUTTONDBLCLK Then
                ' get the control under the mouse
                Dim ss As ISelectionService = CType(GetService(GetType(ISelectionService)), ISelectionService)

                If TypeOf ss.PrimarySelection Is Wizard Then
                    Dim wizard As Wizard = CType(ss.PrimarySelection, Wizard)

                    ' extract the mouse position
                    Dim xPos As Integer = CShort(CUInt(msg.LParam) And &HFFFF)
                    Dim yPos As Integer = CShort((CUInt(msg.LParam) And &HFFFF0000L) >> 16)
                    Dim mousePos As New Point(xPos, yPos)

                    If msg.HWnd = wizard._NextButton.Handle Then
                        If wizard._NextButton.Enabled AndAlso wizard._NextButton.ClientRectangle.Contains(mousePos) Then
                            'Press the button
                            wizard.Next()
                        End If
                    ElseIf msg.HWnd = wizard._BackButton.Handle Then
                        If wizard._BackButton.Enabled AndAlso wizard._BackButton.ClientRectangle.Contains(mousePos) Then
                            'Press the button
                            wizard.Back()
                        End If
                    End If

                    ' filter message
                    Return
                End If
            End If

            ' forward message
            MyBase.WndProc(msg)
        End Sub

        ''' <summary>
        ''' Prevents the grid from being drawn on the Wizard.
        ''' </summary>
        Protected Overrides Property DrawGrid() As Boolean
            Get
                Return False
            End Get
            Set(value As Boolean)
                MyBase.DrawGrid = value
            End Set
        End Property

#End Region

    End Class

#End Region

End Class

