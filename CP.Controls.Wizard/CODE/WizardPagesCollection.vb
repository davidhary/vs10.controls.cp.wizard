#Region "Copyright �2005, Cristi Potlog - All Rights Reserved"
' ------------------------------------------------------------------- *
'*                            Cristi Potlog                             *
'*                  Copyright �2005 - All Rights reserved               *
'*                                                                      *
'* THIS SOURCE CODE IS PROVIDED "AS IS" WITH NO WARRANTIES OF ANY KIND, *
'* EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE        *
'* WARRANTIES OF DESIGN, MERCHANTIBILITY AND FITNESS FOR A PARTICULAR   *
'* PURPOSE, NONINFRINGEMENT, OR ARISING FROM A COURSE OF DEALING,       *
'* USAGE OR TRADE PRACTICE.                                             *
'*                                                                      *
'* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.             *
'* ------------------------------------------------------------------- 
#End Region

#Region " REFERENCES "
Imports System.Collections
#End Region

''' <summary>
''' Represents a collection of wizard pages.
''' </summary>
''' <license>
''' (c) 2012 Integrated Scientific Resources, Inc., All Rights Reserved
''' (c) 2005 Cristi Potlog - All Rights Reserved
''' Licensed under the MIT License. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="09/19/2012" by="David" revision="1.05.4645.x">
''' Based on http://www.CodeProject.com/Articles/10808/Cristi-Potlog-s-Wizard-Control-for-NET
''' </history>
<System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1010:CollectionsShouldImplementGenericInterface")>
<System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1058:TypesShouldNotExtendCertainBaseTypes")>
Public Class WizardPagesCollection
    Inherits CollectionBase

#Region " CONSTRUCTOR "

    Private owner As Wizard
    ''' <summary>
    ''' Creates a new instance of the <see cref="WizardPagesCollection"/> class.
    ''' </summary>
    ''' <param name="owner">A Wizard object that owns the collection.</param>
    Friend Sub New(ByVal owner As Wizard)
        MyBase.new()
        Me.owner = owner
    End Sub
#End Region

#Region " INDEXER "
    Default Public Property Item(ByVal index As Integer) As WizardPage
        Get
            Return CType(MyBase.List(index), WizardPage)
        End Get
        Set(ByVal value As WizardPage)
            MyBase.List(index) = value
        End Set
    End Property
#End Region

#Region " METHODS "

    ''' <summary>
    ''' Adds an object to the end of the WizardPagesCollection.
    ''' </summary>
    ''' <param name="value">The WizardPage to be added.
    ''' The value can be null.</param>
    ''' <returns>An Integer value representing the index at which the value has been added.</returns>
    Public Function Add(ByVal value As WizardPage) As Integer
        Dim result As Integer = List.Add(value)
        Return result
    End Function

    ''' <summary>
    ''' Adds the elements of an array of WizardPage objects to the end of the WizardPagesCollection.  
    ''' </summary>
    ''' <param name="pages">An array on WizardPage objects to be added.
    ''' The array itself cannot be null, but it can contain elements that are null.</param>
    Public Sub AddRange(ByVal pages() As WizardPage)
        ' Use external to validate and add each entry
        If pages IsNot Nothing Then
            For Each page As WizardPage In pages
                Me.Add(page)
            Next page
        End If
    End Sub

    ''' <summary>
    ''' Searches for the specified WizardPage and returns the zero-based index
    ''' of the first occurrence in the entire WizardPagesCollection.
    ''' </summary>
    ''' <param name="value">A WizardPage object to locate in the WizardPagesCollection.
    ''' The value can be null.</param>
    Public Function IndexOf(ByVal value As WizardPage) As Integer
        If value Is Nothing Then
            Return -1
        Else
            Return List.IndexOf(value)
        End If
    End Function

    ''' <summary>
    ''' Inserts an element into the WizardPagesCollection at the specified index.
    ''' </summary>
    ''' <param name="index">An Integer value representing the zero-based index at which value should be inserted.</param>
    ''' <param name="value">A WizardPage object to insert. The value can be null.</param>
    Public Sub Insert(ByVal index As Integer, ByVal value As WizardPage)
        If value IsNot Nothing Then
            ' insert the item
            List.Insert(index, value)
        End If
    End Sub

    ''' <summary>
    ''' Removes the first occurrence of a specific object from the WizardPagesCollection.
    ''' </summary>
    ''' <param name="value">A WizardPage object to remove. The value can be null.</param>
    Public Sub Remove(ByVal value As WizardPage)
        If value IsNot Nothing Then
            ' remove the item
            List.Remove(value)
        End If
    End Sub

    ''' <summary>
    ''' Determines whether an element is in the WizardPagesCollection.  
    ''' </summary>
    ''' <param name="value">The WizardPage object to locate. The value can be null.</param>
    ''' <returns>true if item is found in the WizardPagesCollection; otherwise, false.</returns>
    Public Function Contains(ByVal value As WizardPage) As Boolean
        If value Is Nothing Then
            Return False
        Else
            Return List.Contains(value)
        End If
    End Function

    ''' <summary>
    ''' Performs additional custom processes after inserting a new element into the WizardPagesCollection instance.
    ''' </summary>
    ''' <param name="index">The zero-based index at which to insert value.</param>
    ''' <param name="value">The new value of the element at index.</param>
    Protected Overrides Sub OnInsertComplete(ByVal index As Integer, ByVal value As Object)
        ' call base class
        MyBase.OnInsertComplete(index, value)

        ' reset current page index
        Me.owner.SelectedIndex = index
    End Sub

    ''' <summary>
    ''' Performs additional custom processes after removing an element from the System.Collections.CollectionBase instance.
    ''' </summary>
    ''' <param name="index">The zero-based index at which value can be found.</param>
    ''' <param name="value">The value of the element to remove from index.</param>
    Protected Overrides Sub OnRemoveComplete(ByVal index As Integer, ByVal value As Object)
        ' call base class
        MyBase.OnRemoveComplete(index, value)

        ' check if removing current page
        If Me.owner.SelectedIndex = index Then
            ' check if at the end of the list
            If index < InnerList.Count Then
                Me.owner.SelectedIndex = index
            Else
                Me.owner.SelectedIndex = InnerList.Count - 1
            End If
        End If
    End Sub

    ''' <summary>
    ''' Provide the strongly typed member for ICollection.
    ''' </summary>
    Public Sub CopyTo(ByVal array As WizardPage, ByVal index As Integer)
        Me.CopyTo(array, index)
    End Sub

#End Region

End Class
