﻿''' <summary>
''' Provides data for the Page Changed Event of the Wizard control.
''' </summary>
Public Class PageChangedEventArgs
    Inherits EventArgs

#Region " CONSTRUCTOR "

    ''' <summary>
    ''' Creates a new instance of the <see cref="PageChangedEventArgs"/> class.
    ''' </summary>
    ''' <param name="oldIndex">An integer value representing the index of the old page.</param>
    ''' <param name="newIndex">An integer value representing the index of the new page.</param>
    Friend Sub New(ByVal oldIndex As Integer, ByVal newIndex As Integer)
        Me._oldIndex = oldIndex
        Me._InternalNewIndex = newIndex
    End Sub

#End Region

#Region " PROPERTIES "

    Private _oldIndex As Integer
    ''' <summary>
    ''' Gets the index of the old page.
    ''' </summary>
    Public ReadOnly Property OldIndex() As Integer
        Get
            Return Me._oldIndex
        End Get
    End Property

    Protected Property InternalNewIndex As Integer
    ''' <summary>
    ''' Gets or sets the index of the new page.
    ''' </summary>
    Public ReadOnly Property NewIndex() As Integer
        Get
            Return Me._InternalNewIndex
        End Get
    End Property

#End Region

End Class

''' <summary>
''' Provides data for the Page Changing Event of the Wizard control.
''' </summary>
Public Class PageChangingEventArgs
    Inherits PageChangedEventArgs

#Region " CONSTRUCTOR "

    ''' <summary>
    ''' Creates a new instance of the <see cref="PageChangingEventArgs"/> class.
    ''' </summary>
    ''' <param name="oldIndex">An integer value representing the index of the old page.</param>
    ''' <param name="newIndex">An integer value representing the index of the new page.</param>
    Friend Sub New(ByVal oldIndex As Integer, ByVal newIndex As Integer)
        MyBase.New(oldIndex, newIndex)
        ' nothing
    End Sub

#End Region

#Region " PROPERTIES "

    Private _cancel As Boolean = False
    ''' <summary>
    ''' Indicates whether the page change should be canceled.
    ''' </summary>
    Public Property Cancel() As Boolean
        Get
            Return Me._cancel
        End Get
        Set(ByVal value As Boolean)
            Me._cancel = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the index of the new page.
    ''' </summary>
    Public Shadows Property NewIndex() As Integer
        Get
            Return MyBase.InternalNewIndex
        End Get
        Set(ByVal value As Integer)
            MyBase.InternalNewIndex = value
        End Set
    End Property

#End Region

End Class
