#Region "Copyright �2005, Cristi Potlog - All Rights Reserved"
' ------------------------------------------------------------------- *
'*                            Cristi Potlog                             *
'*                  Copyright �2005 - All Rights reserved               *
'*                                                                      *
'* THIS SOURCE CODE IS PROVIDED "AS IS" WITH NO WARRANTIES OF ANY KIND, *
'* EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE        *
'* WARRANTIES OF DESIGN, MERCHANTIBILITY AND FITNESS FOR A PARTICULAR   *
'* PURPOSE, NONINFRINGEMENT, OR ARISING FROM A COURSE OF DEALING,       *
'* USAGE OR TRADE PRACTICE.                                             *
'*                                                                      *
'* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.             *
'* ------------------------------------------------------------------- 
#End Region

#Region " REFERENCES "
Imports System.ComponentModel
Imports System.Windows.Forms.Design
#End Region

<DefaultEvent("Click"), Designer(GetType(WizardPage.WizardPageDesigner))> 
Partial Class WizardPage

End Class
