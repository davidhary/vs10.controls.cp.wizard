﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> 
Partial Class SimpleWizard
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> 
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> 
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me._LicenseWizardPage = New CP.Controls.WizardPage()
        Me._AgreeCheckBox = New System.Windows.Forms.CheckBox()
        Me._LicenseTextBox = New System.Windows.Forms.TextBox()
        Me._OptionsWizardPage = New CP.Controls.WizardPage()
        Me._OptionsGroupBox = New System.Windows.Forms.GroupBox()
        Me._SkipOptionRadioButton = New System.Windows.Forms.RadioButton()
        Me._CheckOptionRadioButton = New System.Windows.Forms.RadioButton()
        Me._FinishWizardPage = New CP.Controls.WizardPage()
        Me._SampleWizard = New CP.Controls.Wizard()
        Me._CheckWizardPage = New CP.Controls.WizardPage()
        Me._PlaceholderLabel = New System.Windows.Forms.Label()
        Me._ProgressWizardPage = New CP.Controls.WizardPage()
        Me._ProgressLabel = New System.Windows.Forms.Label()
        Me._LongTaskProgressBar = New System.Windows.Forms.ProgressBar()
        Me._WelcomeWizardPage = New CP.Controls.WizardPage()
        Me._LongTaskTimer = New System.Windows.Forms.Timer(Me.components)
        Me._LicenseWizardPage.SuspendLayout()
        Me._OptionsWizardPage.SuspendLayout()
        Me._OptionsGroupBox.SuspendLayout()
        Me._SampleWizard.SuspendLayout()
        Me._CheckWizardPage.SuspendLayout()
        Me._ProgressWizardPage.SuspendLayout()
        Me.SuspendLayout()
        '
        '_LicenseWizardPage
        '
        Me._LicenseWizardPage.Controls.Add(Me._AgreeCheckBox)
        Me._LicenseWizardPage.Controls.Add(Me._LicenseTextBox)
        Me._LicenseWizardPage.Description = "Please read the following license agreement and confirm that you agree with all terms and conditions."
        Me._LicenseWizardPage.Location = New System.Drawing.Point(0, 0)
        Me._LicenseWizardPage.Name = "_LicenseWizardPage"
        Me._LicenseWizardPage.Size = New System.Drawing.Size(466, 296)
        Me._LicenseWizardPage.TabIndex = 10
        Me._LicenseWizardPage.Title = "License Agreement"
        Me._LicenseWizardPage.WizardPageStyle = CP.Controls.WizardPageStyle.Standard
        '
        '_AgreeCheckBox
        '
        Me._AgreeCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._AgreeCheckBox.Location = New System.Drawing.Point(12, 272)
        Me._AgreeCheckBox.Name = "_AgreeCheckBox"
        Me._AgreeCheckBox.Size = New System.Drawing.Size(288, 16)
        Me._AgreeCheckBox.TabIndex = 0
        Me._AgreeCheckBox.Text = "I agree with this license's terms and conditions."
        '
        '_LicenseTextBox
        '
        Me._LicenseTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._LicenseTextBox.Location = New System.Drawing.Point(12, 76)
        Me._LicenseTextBox.Multiline = True
        Me._LicenseTextBox.Name = "_LicenseTextBox"
        Me._LicenseTextBox.ReadOnly = True
        Me._LicenseTextBox.Size = New System.Drawing.Size(440, 188)
        Me._LicenseTextBox.TabIndex = 1
        Me._LicenseTextBox.Text = "Some long and boring license text..."
        '
        '_OptionsWizardPage
        '
        Me._OptionsWizardPage.Controls.Add(Me._OptionsGroupBox)
        Me._OptionsWizardPage.Description = "Please select an option from the available list."
        Me._OptionsWizardPage.Location = New System.Drawing.Point(0, 0)
        Me._OptionsWizardPage.Name = "_OptionsWizardPage"
        Me._OptionsWizardPage.Size = New System.Drawing.Size(466, 296)
        Me._OptionsWizardPage.TabIndex = 11
        Me._OptionsWizardPage.Title = "Task Options"
        Me._OptionsWizardPage.WizardPageStyle = CP.Controls.WizardPageStyle.Standard
        '
        '_OptionsGroupBox
        '
        Me._OptionsGroupBox.Controls.Add(Me._SkipOptionRadioButton)
        Me._OptionsGroupBox.Controls.Add(Me._CheckOptionRadioButton)
        Me._OptionsGroupBox.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._OptionsGroupBox.Location = New System.Drawing.Point(16, 84)
        Me._OptionsGroupBox.Name = "_OptionsGroupBox"
        Me._OptionsGroupBox.Size = New System.Drawing.Size(436, 112)
        Me._OptionsGroupBox.TabIndex = 0
        Me._OptionsGroupBox.TabStop = False
        Me._OptionsGroupBox.Text = "Available Options"
        '
        '_SkipOptionRadioButton
        '
        Me._SkipOptionRadioButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._SkipOptionRadioButton.Location = New System.Drawing.Point(20, 36)
        Me._SkipOptionRadioButton.Name = "_SkipOptionRadioButton"
        Me._SkipOptionRadioButton.Size = New System.Drawing.Size(260, 20)
        Me._SkipOptionRadioButton.TabIndex = 0
        Me._SkipOptionRadioButton.Text = "Skip any checks and proceed."
        '
        '_CheckOptionRadioButton
        '
        Me._CheckOptionRadioButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._CheckOptionRadioButton.Location = New System.Drawing.Point(20, 72)
        Me._CheckOptionRadioButton.Name = "_CheckOptionRadioButton"
        Me._CheckOptionRadioButton.Size = New System.Drawing.Size(260, 20)
        Me._CheckOptionRadioButton.TabIndex = 1
        Me._CheckOptionRadioButton.Text = "Check for something first."
        '
        '_FinishWizardPage
        '
        Me._FinishWizardPage.Description = "Thank you for using the Sample Wizard." & Global.Microsoft.VisualBasic.ChrW(10) & "Press OK to exit."
        Me._FinishWizardPage.Location = New System.Drawing.Point(0, 0)
        Me._FinishWizardPage.Name = "_FinishWizardPage"
        Me._FinishWizardPage.Size = New System.Drawing.Size(466, 296)
        Me._FinishWizardPage.TabIndex = 12
        Me._FinishWizardPage.Title = "Sample Wizard has finished"
        Me._FinishWizardPage.WizardPageStyle = CP.Controls.WizardPageStyle.Finish
        '
        '_SampleWizard
        '
        Me._SampleWizard.Controls.Add(Me._FinishWizardPage)
        Me._SampleWizard.Controls.Add(Me._ProgressWizardPage)
        Me._SampleWizard.Controls.Add(Me._CheckWizardPage)
        Me._SampleWizard.Controls.Add(Me._OptionsWizardPage)
        Me._SampleWizard.Controls.Add(Me._LicenseWizardPage)
        Me._SampleWizard.Controls.Add(Me._WelcomeWizardPage)
        Me._SampleWizard.Dock = System.Windows.Forms.DockStyle.None
        Me._SampleWizard.FinishText = "&Finish"
        Me._SampleWizard.HeaderImage = Global.CP.Controls.My.Resources.Resources.HeaderIcon
        Me._SampleWizard.HelpVisible = True
        Me._SampleWizard.Location = New System.Drawing.Point(0, 0)
        Me._SampleWizard.Name = "_SampleWizard"
        Me._SampleWizard.Pages.AddRange(New CP.Controls.WizardPage() {Me._WelcomeWizardPage, Me._LicenseWizardPage, Me._OptionsWizardPage, Me._CheckWizardPage, Me._ProgressWizardPage, Me._FinishWizardPage})
        Me._SampleWizard.Size = New System.Drawing.Size(466, 344)
        Me._SampleWizard.TabIndex = 0
        Me._SampleWizard.WelcomeImage = Global.CP.Controls.My.Resources.Resources.WelcomeImage
        '
        '_CheckWizardPage
        '
        Me._CheckWizardPage.Controls.Add(Me._PlaceholderLabel)
        Me._CheckWizardPage.Description = "Please enter required information."
        Me._CheckWizardPage.Location = New System.Drawing.Point(0, 0)
        Me._CheckWizardPage.Name = "_CheckWizardPage"
        Me._CheckWizardPage.Size = New System.Drawing.Size(466, 296)
        Me._CheckWizardPage.TabIndex = 13
        Me._CheckWizardPage.Title = "Check Something"
        Me._CheckWizardPage.WizardPageStyle = CP.Controls.WizardPageStyle.Standard
        '
        '_PlaceholderLabel
        '
        Me._PlaceholderLabel.ForeColor = System.Drawing.Color.Red
        Me._PlaceholderLabel.Location = New System.Drawing.Point(28, 100)
        Me._PlaceholderLabel.Name = "_PlaceholderLabel"
        Me._PlaceholderLabel.Size = New System.Drawing.Size(384, 16)
        Me._PlaceholderLabel.TabIndex = 0
        Me._PlaceholderLabel.Text = "Place some validation controls here."
        '
        '_ProgressWizardPage
        '
        Me._ProgressWizardPage.Controls.Add(Me._ProgressLabel)
        Me._ProgressWizardPage.Controls.Add(Me._LongTaskProgressBar)
        Me._ProgressWizardPage.Description = "This simulates a long running sample task."
        Me._ProgressWizardPage.Location = New System.Drawing.Point(0, 0)
        Me._ProgressWizardPage.Name = "_ProgressWizardPage"
        Me._ProgressWizardPage.Size = New System.Drawing.Size(466, 296)
        Me._ProgressWizardPage.TabIndex = 10
        Me._ProgressWizardPage.Title = "Task Running"
        Me._ProgressWizardPage.WizardPageStyle = CP.Controls.WizardPageStyle.Standard
        '
        '_ProgressLabel
        '
        Me._ProgressLabel.Location = New System.Drawing.Point(20, 84)
        Me._ProgressLabel.Name = "_ProgressLabel"
        Me._ProgressLabel.Size = New System.Drawing.Size(252, 16)
        Me._ProgressLabel.TabIndex = 1
        Me._ProgressLabel.Text = "Please wait while the wizard does a long task..."
        '
        '_LongTaskProgressBar
        '
        Me._LongTaskProgressBar.Location = New System.Drawing.Point(16, 104)
        Me._LongTaskProgressBar.Name = "_LongTaskProgressBar"
        Me._LongTaskProgressBar.Size = New System.Drawing.Size(436, 20)
        Me._LongTaskProgressBar.TabIndex = 0
        '
        '_WelcomeWizardPage
        '
        Me._WelcomeWizardPage.Description = "This wizard will guide you through the steps of performing a sample task."
        Me._WelcomeWizardPage.Location = New System.Drawing.Point(0, 0)
        Me._WelcomeWizardPage.Name = "_WelcomeWizardPage"
        Me._WelcomeWizardPage.Size = New System.Drawing.Size(466, 296)
        Me._WelcomeWizardPage.TabIndex = 9
        Me._WelcomeWizardPage.Title = "Welcome to the Sample  Wizard"
        Me._WelcomeWizardPage.WizardPageStyle = CP.Controls.WizardPageStyle.Welcome
        '
        '_LongTaskTimer
        '
        '
        'SimpleWizard
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(468, 338)
        Me.Controls.Add(Me._SampleWizard)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "SimpleWizard"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Simple Wizard"
        Me._LicenseWizardPage.ResumeLayout(False)
        Me._LicenseWizardPage.PerformLayout()
        Me._OptionsWizardPage.ResumeLayout(False)
        Me._OptionsGroupBox.ResumeLayout(False)
        Me._SampleWizard.ResumeLayout(False)
        Me._CheckWizardPage.ResumeLayout(False)
        Me._ProgressWizardPage.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Private WithEvents _AgreeCheckBox As System.Windows.Forms.CheckBox
    Private _LicenseTextBox As System.Windows.Forms.TextBox
    Private _LicenseWizardPage As WizardPage
    Private _OptionsWizardPage As WizardPage
    Private _FinishWizardPage As WizardPage
    Private _WelcomeWizardPage As WizardPage
    Private _ProgressWizardPage As WizardPage
    Private WithEvents _SampleWizard As Global.CP.Controls.Wizard
    Private _LongTaskProgressBar As System.Windows.Forms.ProgressBar
    Private _ProgressLabel As System.Windows.Forms.Label
    Private WithEvents _LongTaskTimer As System.Windows.Forms.Timer
    Private _OptionsGroupBox As System.Windows.Forms.GroupBox
    Private _SkipOptionRadioButton As System.Windows.Forms.RadioButton
    Private _CheckOptionRadioButton As System.Windows.Forms.RadioButton
    Private _CheckWizardPage As WizardPage
    Private _PlaceholderLabel As System.Windows.Forms.Label


End Class
